# DocUploadLib

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.12.

## Code scaffolding

Run `ng generate component component-name --project doc-upload-lib` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project doc-upload-lib`.
> Note: Don't forget to add `--project doc-upload-lib` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build doc-upload-lib` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build doc-upload-lib`, go to the dist folder `cd dist/doc-upload-lib` and run `npm publish`.

## Running unit tests

Run `ng test doc-upload-lib` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
