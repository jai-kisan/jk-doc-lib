import { Component, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
const _c0 = ["progressbar"];
const _c1 = ["docwrapper"];
function DocUploadLibComponent_span_10_Template(rf, ctx) { if (rf & 1) {
    const _r9 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span", 12);
    i0.ɵɵlistener("click", function DocUploadLibComponent_span_10_Template_span_click_0_listener() { i0.ɵɵrestoreView(_r9); i0.ɵɵnextContext(); const _r6 = i0.ɵɵreference(16); return _r6.click(); });
    i0.ɵɵelementStart(1, "i", 13);
    i0.ɵɵtext(2, "cloud_upload");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function DocUploadLibComponent_span_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "i", 13);
    i0.ɵɵtext(2, "cloud_download");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function DocUploadLibComponent_span_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "i", 14);
    i0.ɵɵtext(2, "cancel");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function DocUploadLibComponent_span_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "i", 14);
    i0.ɵɵtext(2, "delete");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function DocUploadLibComponent_span_14_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "i", 15);
    i0.ɵɵtext(2, "check_circle");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function DocUploadLibComponent_div_17_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 16);
    i0.ɵɵelement(1, "div", 17);
    i0.ɵɵelementEnd();
} }
export class DocUploadLibComponent {
    constructor() {
        this.isUploadNeeded = true;
        this.isDeleteAllowed = true;
        this.isDocVerified = false;
        this.emitFile = new EventEmitter();
        this.emitDocUrl = new EventEmitter();
    }
    ngOnInit() {
    }
    ngAfterViewInit() {
        this.inputWidth = this.docWrapper.nativeElement.innerWidth;
    }
    uploadFile(event) {
        this.emitFile.emit(event.target.files[0]);
    }
    onDownload() {
        this.emitDocUrl.emit(this.fileDownloadString);
    }
}
DocUploadLibComponent.ɵfac = function DocUploadLibComponent_Factory(t) { return new (t || DocUploadLibComponent)(); };
DocUploadLibComponent.ɵcmp = i0.ɵɵdefineComponent({ type: DocUploadLibComponent, selectors: [["doc-doc-upload-lib"]], viewQuery: function DocUploadLibComponent_Query(rf, ctx) { if (rf & 1) {
        i0.ɵɵviewQuery(_c0, true);
        i0.ɵɵviewQuery(_c1, true);
    } if (rf & 2) {
        var _t;
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.elementProgress = _t.first);
        i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.docWrapper = _t.first);
    } }, inputs: { isUploadNeeded: "isUploadNeeded", isDeleteAllowed: "isDeleteAllowed", isDocVerified: "isDocVerified", fileName: "fileName", fileUploadStatus: "fileUploadStatus", fileDownloadString: "fileDownloadString", accept: "accept" }, outputs: { emitFile: "emitFile", emitDocUrl: "emitDocUrl" }, decls: 18, vars: 10, consts: [[1, "upload-btn-wrapper"], [1, "upload-btn"], ["docwrapper", ""], [1, "file-icon", 3, "click"], [1, "material-icons-outlined", 2, "color", "rgba(0, 0, 0, 0.54)"], [1, "file-name", 3, "click"], [1, "btn", 3, "ngClass"], [3, "click", 4, "ngIf"], [4, "ngIf"], ["type", "file", "name", "fieldName", 1, "input", 3, "disabled", "accept", "change"], ["file", ""], ["class", "progress", 4, "ngIf"], [3, "click"], [1, "material-icons-outlined", 2, "color", "#2196F3"], [1, "material-icons", 2, "color", "#E65100"], [1, "material-icons", 2, "color", "#25BD17"], [1, "progress"], [1, "indeterminate"]], template: function DocUploadLibComponent_Template(rf, ctx) { if (rf & 1) {
        const _r10 = i0.ɵɵgetCurrentView();
        i0.ɵɵelementStart(0, "div", 0);
        i0.ɵɵelementStart(1, "div", 1, 2);
        i0.ɵɵelementStart(3, "div", 3);
        i0.ɵɵlistener("click", function DocUploadLibComponent_Template_div_click_3_listener() { i0.ɵɵrestoreView(_r10); const _r6 = i0.ɵɵreference(16); return _r6.click(); });
        i0.ɵɵelementStart(4, "span");
        i0.ɵɵelementStart(5, "i", 4);
        i0.ɵɵtext(6, " insert_photo ");
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(7, "div", 5);
        i0.ɵɵlistener("click", function DocUploadLibComponent_Template_div_click_7_listener() { i0.ɵɵrestoreView(_r10); const _r6 = i0.ɵɵreference(16); return _r6.click(); });
        i0.ɵɵtext(8);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(9, "div", 6);
        i0.ɵɵtemplate(10, DocUploadLibComponent_span_10_Template, 3, 0, "span", 7);
        i0.ɵɵtemplate(11, DocUploadLibComponent_span_11_Template, 3, 0, "span", 8);
        i0.ɵɵtemplate(12, DocUploadLibComponent_span_12_Template, 3, 0, "span", 8);
        i0.ɵɵtemplate(13, DocUploadLibComponent_span_13_Template, 3, 0, "span", 8);
        i0.ɵɵtemplate(14, DocUploadLibComponent_span_14_Template, 3, 0, "span", 8);
        i0.ɵɵelementEnd();
        i0.ɵɵelementStart(15, "input", 9, 10);
        i0.ɵɵlistener("change", function DocUploadLibComponent_Template_input_change_15_listener($event) { return ctx.uploadFile($event); });
        i0.ɵɵelementEnd();
        i0.ɵɵelementEnd();
        i0.ɵɵtemplate(17, DocUploadLibComponent_div_17_Template, 2, 0, "div", 11);
        i0.ɵɵelementEnd();
    } if (rf & 2) {
        i0.ɵɵadvance(8);
        i0.ɵɵtextInterpolate1(" ", ctx.fileName, " ");
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngClass", ctx.fileUploadStatus);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.isUploadNeeded);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.fileDownloadString);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.fileUploadStatus === "uploading");
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.fileDownloadString && ctx.isDeleteAllowed);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("ngIf", ctx.fileDownloadString && ctx.isDocVerified);
        i0.ɵɵadvance(1);
        i0.ɵɵproperty("disabled", !ctx.isUploadNeeded)("accept", ctx.accept);
        i0.ɵɵadvance(2);
        i0.ɵɵproperty("ngIf", ctx.fileUploadStatus === "uploading");
    } }, directives: [i1.NgClass, i1.NgIf], styles: [".upload-btn-wrapper[_ngcontent-%COMP%]{\n      position: relative;\n      display: inline-block;\n    }\n    .upload-btn[_ngcontent-%COMP%] {\n      position: relative;\n      overflow: hidden;\n      display: inline-flex;\n      flex-direction: row;\n      margin: 5px;\n      cursor: pointer;\n      background: rgba(0, 0, 0, 0.04);\n      border-radius: 4px;\n      height: 40px;\n      padding: 7px;\n      box-sizing: border-box;\n      background: #E3F2FD;\n    }\n    .file-icon[_ngcontent-%COMP%] {\n      margin-right: 5px;\n      align-self: center;\n      z-index: 1;\n    }\n    .file-icon[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n      font-size: 26px;\n      height: 26px;\n      width: 26px;\n    }\n    .file-name[_ngcontent-%COMP%] {\n      margin-right: 5px;\n      text-overflow: ellipsis;\n      align-self: center;\n      flex: 1;\n      box-sizing: border-box;\n      padding-bottom: 4px;\n      font-size: 12px;\n      line-height: 20px;\n      letter-spacing: 0.25px;\n      color: #828282;\n      mix-blend-mode: normal;\n      z-index: 1;\n    }\n  .upload-btn[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%] {\n    align-self: center;\n      margin-left: 5px;\n      color: #2196f3;\n      z-index: 1;\n  }\n  .upload-btn[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{\n    margin: 5px;\n    z-index: 1;\n  }\n  \n  .upload-anim[_ngcontent-%COMP%]{\n    height: 2px;\n    position: relative;\n    left: 0px;\n    top: -8px;\n    border-radius: 4px;\n    box-sizing: border-box;\n    height: 2px;\n    background-image: linear-gradient(\n      90deg,\n      rgba(227, 242, 253, 1) 0%,\n      rgba(100, 181, 246, 1) 45%,\n      rgba(100, 181, 246, 1) 45%\n    );\n    animation: move 2s linear infinite;\n  }\n  @keyframes move {\n    0% {\n      background-position: 0 0;\n    }\n    100% {\n      background-position: 250px 250px;\n    }\n  }\n  .input[_ngcontent-%COMP%] {\n      font-size: 100px;\n      position: absolute;\n      left: 0;\n      top: 0;\n      opacity: 0;\n  }\n  .progress[_ngcontent-%COMP%] {\n    position: relative;\n    top: -7px;\n    height: 2px;\n    display: block;\n    width: calc( 100% - 10px );\n    border-radius: 2px;\n    box-sizing: border-box;\n    background-clip: padding-box;\n    overflow: hidden;\n    margin-left: 5px; \n  }\n  .progress[_ngcontent-%COMP%]   .indeterminate[_ngcontent-%COMP%] {\n      background-color: #2196F3; \n  }\n  .progress[_ngcontent-%COMP%]   .indeterminate[_ngcontent-%COMP%]:before {\n    content: '';\n    position: absolute;\n    background-color: inherit;\n    top: 0;\n    left: 0;\n    bottom: 0;\n    will-change: left, right;\n    -webkit-animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite;\n            animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite; }\n.progress[_ngcontent-%COMP%]   .indeterminate[_ngcontent-%COMP%]:after {\n    content: '';\n    position: absolute;\n    background-color: inherit;\n    top: 0;\n    left: 0;\n    bottom: 0;\n    will-change: left, right;\n    -webkit-animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n            animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n    -webkit-animation-delay: 1.15s;\n            animation-delay: 1.15s; }\n            @-webkit-keyframes indeterminate {\n              0% {\n                left: -35%;\n                right: 100%; }\n              60% {\n                left: 100%;\n                right: -90%; }\n              100% {\n                left: 100%;\n                right: -90%; } }\n            @keyframes indeterminate {\n              0% {\n                left: -35%;\n                right: 100%; }\n              60% {\n                left: 100%;\n                right: -90%; }\n              100% {\n                left: 100%;\n                right: -90%; } }\n            @-webkit-keyframes indeterminate-short {\n              0% {\n                left: -200%;\n                right: 100%; }\n              60% {\n                left: 107%;\n                right: -8%; }\n              100% {\n                left: 107%;\n                right: -8%; } }\n            @keyframes indeterminate-short {\n              0% {\n                left: -200%;\n                right: 100%; }\n              60% {\n                left: 107%;\n                right: -8%; }\n              100% {\n                left: 107%;\n                right: -8%; } }"] });
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(DocUploadLibComponent, [{
        type: Component,
        args: [{
                selector: 'doc-doc-upload-lib',
                templateUrl: './doc-upload-lib.component.html',
                styles: [
                    `.upload-btn-wrapper{
      position: relative;
      display: inline-block;
    }
    .upload-btn {
      position: relative;
      overflow: hidden;
      display: inline-flex;
      flex-direction: row;
      margin: 5px;
      cursor: pointer;
      background: rgba(0, 0, 0, 0.04);
      border-radius: 4px;
      height: 40px;
      padding: 7px;
      box-sizing: border-box;
      background: #E3F2FD;
    }
    .file-icon {
      margin-right: 5px;
      align-self: center;
      z-index: 1;
    }
    .file-icon i {
      font-size: 26px;
      height: 26px;
      width: 26px;
    }
    .file-name {
      margin-right: 5px;
      text-overflow: ellipsis;
      align-self: center;
      flex: 1;
      box-sizing: border-box;
      padding-bottom: 4px;
      font-size: 12px;
      line-height: 20px;
      letter-spacing: 0.25px;
      color: #828282;
      mix-blend-mode: normal;
      z-index: 1;
    }
  .upload-btn .btn {
    align-self: center;
      margin-left: 5px;
      color: #2196f3;
      z-index: 1;
  }
  .upload-btn .btn span{
    margin: 5px;
    z-index: 1;
  }
  
  .upload-anim{
    height: 2px;
    position: relative;
    left: 0px;
    top: -8px;
    border-radius: 4px;
    box-sizing: border-box;
    height: 2px;
    background-image: linear-gradient(
      90deg,
      rgba(227, 242, 253, 1) 0%,
      rgba(100, 181, 246, 1) 45%,
      rgba(100, 181, 246, 1) 45%
    );
    animation: move 2s linear infinite;
  }
  @keyframes move {
    0% {
      background-position: 0 0;
    }
    100% {
      background-position: 250px 250px;
    }
  }
  .input {
      font-size: 100px;
      position: absolute;
      left: 0;
      top: 0;
      opacity: 0;
  }
  .progress {
    position: relative;
    top: -7px;
    height: 2px;
    display: block;
    width: calc( 100% - 10px );
    border-radius: 2px;
    box-sizing: border-box;
    background-clip: padding-box;
    overflow: hidden;
    margin-left: 5px; 
  }
  .progress .indeterminate {
      background-color: #2196F3; 
  }
  .progress .indeterminate:before {
    content: '';
    position: absolute;
    background-color: inherit;
    top: 0;
    left: 0;
    bottom: 0;
    will-change: left, right;
    -webkit-animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite;
            animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite; }
.progress .indeterminate:after {
    content: '';
    position: absolute;
    background-color: inherit;
    top: 0;
    left: 0;
    bottom: 0;
    will-change: left, right;
    -webkit-animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;
            animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;
    -webkit-animation-delay: 1.15s;
            animation-delay: 1.15s; }
            @-webkit-keyframes indeterminate {
              0% {
                left: -35%;
                right: 100%; }
              60% {
                left: 100%;
                right: -90%; }
              100% {
                left: 100%;
                right: -90%; } }
            @keyframes indeterminate {
              0% {
                left: -35%;
                right: 100%; }
              60% {
                left: 100%;
                right: -90%; }
              100% {
                left: 100%;
                right: -90%; } }
            @-webkit-keyframes indeterminate-short {
              0% {
                left: -200%;
                right: 100%; }
              60% {
                left: 107%;
                right: -8%; }
              100% {
                left: 107%;
                right: -8%; } }
            @keyframes indeterminate-short {
              0% {
                left: -200%;
                right: 100%; }
              60% {
                left: 107%;
                right: -8%; }
              100% {
                left: 107%;
                right: -8%; } }
  `
                ]
            }]
    }], function () { return []; }, { elementProgress: [{
            type: ViewChild,
            args: ['progressbar', { static: false }]
        }], docWrapper: [{
            type: ViewChild,
            args: ['docwrapper', { static: false }]
        }], isUploadNeeded: [{
            type: Input
        }], isDeleteAllowed: [{
            type: Input
        }], isDocVerified: [{
            type: Input
        }], fileName: [{
            type: Input
        }], fileUploadStatus: [{
            type: Input
        }], fileDownloadString: [{
            type: Input
        }], accept: [{
            type: Input
        }], emitFile: [{
            type: Output
        }], emitDocUrl: [{
            type: Output
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG9jLXVwbG9hZC1saWIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vZG9jLXVwbG9hZC1saWIvIiwic291cmNlcyI6WyJsaWIvZG9jLXVwbG9hZC1saWIuY29tcG9uZW50LnRzIiwibGliL2RvYy11cGxvYWQtbGliLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsU0FBUyxFQUFjLE1BQU0sRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7Ozs7O0lDYzFGLGdDQUNJO0lBRHlCLG1MQUFTLFdBQWlCLElBQUM7SUFDcEQsNkJBQTJEO0lBQUEsNEJBQVk7SUFBQSxpQkFBSTtJQUMvRSxpQkFBTzs7O0lBQ1AsNEJBQ0k7SUFBQSw2QkFBMkQ7SUFBQSw4QkFBYztJQUFBLGlCQUFJO0lBQ2pGLGlCQUFPOzs7SUFDUCw0QkFDSTtJQUFBLDZCQUFrRDtJQUFBLHNCQUFNO0lBQUEsaUJBQUk7SUFDaEUsaUJBQU87OztJQUNQLDRCQUNJO0lBQUEsNkJBQWtEO0lBQUEsc0JBQU07SUFBQSxpQkFBSTtJQUNoRSxpQkFBTzs7O0lBQ1AsNEJBQ0k7SUFBQSw2QkFBa0Q7SUFBQSw0QkFBWTtJQUFBLGlCQUFJO0lBQ3RFLGlCQUFPOzs7SUFLZiwrQkFDSTtJQUFBLDBCQUFpQztJQUNyQyxpQkFBTTs7QUR1SVYsTUFBTSxPQUFPLHFCQUFxQjtJQTRCaEM7UUFwQkEsbUJBQWMsR0FBWSxJQUFJLENBQUM7UUFFL0Isb0JBQWUsR0FBWSxJQUFJLENBQUM7UUFFaEMsa0JBQWEsR0FBWSxLQUFLLENBQUM7UUFXL0IsYUFBUSxHQUF1QixJQUFJLFlBQVksRUFBRSxDQUFDO1FBR2xELGVBQVUsR0FBeUIsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQUV0QyxDQUFDO0lBRWpCLFFBQVE7SUFDUixDQUFDO0lBRUQsZUFBZTtRQUNiLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDO0lBQzdELENBQUM7SUFHRCxVQUFVLENBQUMsS0FBVTtRQUNuQixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFRCxVQUFVO1FBQ1IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7SUFDaEQsQ0FBQzs7MEZBNUNVLHFCQUFxQjswREFBckIscUJBQXFCOzs7Ozs7Ozs7UUMxS2xDLDhCQUNJO1FBQUEsaUNBQ0k7UUFBQSw4QkFDSTtRQURtQix1SkFBUyxXQUFpQixJQUFDO1FBRTlDLDRCQUNJO1FBQUEsNEJBQ0k7UUFBQSw4QkFDSjtRQUFBLGlCQUFJO1FBQ1IsaUJBQU87UUFDWCxpQkFBTTtRQUNOLDhCQUNJO1FBRG1CLHVKQUFTLFdBQWlCLElBQUM7UUFDOUMsWUFDSjtRQUFBLGlCQUFNO1FBQ04sOEJBQ0k7UUFBQSwwRUFDSTtRQUVKLDBFQUNJO1FBRUosMEVBQ0k7UUFFSiwwRUFDSTtRQUVKLDBFQUNJO1FBRVIsaUJBQU07UUFDTixxQ0FFSjtRQUZzRywwR0FBVSxzQkFBa0IsSUFBQztRQUEvSCxpQkFFSjtRQUFBLGlCQUFNO1FBQ04seUVBQ0k7UUFFUixpQkFBTTs7UUF6Qk0sZUFDSjtRQURJLDZDQUNKO1FBQ2lCLGVBQTRCO1FBQTVCLDhDQUE0QjtRQUNuQyxlQUFzQjtRQUF0Qix5Q0FBc0I7UUFHdEIsZUFBMEI7UUFBMUIsNkNBQTBCO1FBRzFCLGVBQXdDO1FBQXhDLDJEQUF3QztRQUd4QyxlQUE2QztRQUE3QyxvRUFBNkM7UUFHN0MsZUFBMkM7UUFBM0Msa0VBQTJDO1FBSUYsZUFBNEI7UUFBNUIsOENBQTRCLHNCQUFBO1FBRzdELGVBQXdDO1FBQXhDLDJEQUF3Qzs7a0REeUlyRCxxQkFBcUI7Y0F4S2pDLFNBQVM7ZUFBQztnQkFDVCxRQUFRLEVBQUUsb0JBQW9CO2dCQUM5QixXQUFXLEVBQUUsaUNBQWlDO2dCQUM5QyxNQUFNLEVBQUU7b0JBQ047Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBaUtEO2lCQUNBO2FBQ0Y7O2tCQUtFLFNBQVM7bUJBQUMsYUFBYSxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRTs7a0JBQzFDLFNBQVM7bUJBQUMsWUFBWSxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRTs7a0JBRXpDLEtBQUs7O2tCQUVMLEtBQUs7O2tCQUVMLEtBQUs7O2tCQUVMLEtBQUs7O2tCQUVMLEtBQUs7O2tCQUVMLEtBQUs7O2tCQUVMLEtBQUs7O2tCQUdMLE1BQU07O2tCQUdOLE1BQU0iLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0NoaWxkLCBFbGVtZW50UmVmLCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnZG9jLWRvYy11cGxvYWQtbGliJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2RvYy11cGxvYWQtbGliLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVzOiBbXG4gICAgYC51cGxvYWQtYnRuLXdyYXBwZXJ7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgfVxuICAgIC51cGxvYWQtYnRuIHtcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICBtYXJnaW46IDVweDtcbiAgICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICAgIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC4wNCk7XG4gICAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgICBoZWlnaHQ6IDQwcHg7XG4gICAgICBwYWRkaW5nOiA3cHg7XG4gICAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgICAgYmFja2dyb3VuZDogI0UzRjJGRDtcbiAgICB9XG4gICAgLmZpbGUtaWNvbiB7XG4gICAgICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgICAgIHotaW5kZXg6IDE7XG4gICAgfVxuICAgIC5maWxlLWljb24gaSB7XG4gICAgICBmb250LXNpemU6IDI2cHg7XG4gICAgICBoZWlnaHQ6IDI2cHg7XG4gICAgICB3aWR0aDogMjZweDtcbiAgICB9XG4gICAgLmZpbGUtbmFtZSB7XG4gICAgICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgICAgIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xuICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgICAgZmxleDogMTtcbiAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgICBwYWRkaW5nLWJvdHRvbTogNHB4O1xuICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgICAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gICAgICBsZXR0ZXItc3BhY2luZzogMC4yNXB4O1xuICAgICAgY29sb3I6ICM4MjgyODI7XG4gICAgICBtaXgtYmxlbmQtbW9kZTogbm9ybWFsO1xuICAgICAgei1pbmRleDogMTtcbiAgICB9XG4gIC51cGxvYWQtYnRuIC5idG4ge1xuICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7XG4gICAgICBjb2xvcjogIzIxOTZmMztcbiAgICAgIHotaW5kZXg6IDE7XG4gIH1cbiAgLnVwbG9hZC1idG4gLmJ0biBzcGFue1xuICAgIG1hcmdpbjogNXB4O1xuICAgIHotaW5kZXg6IDE7XG4gIH1cbiAgXG4gIC51cGxvYWQtYW5pbXtcbiAgICBoZWlnaHQ6IDJweDtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgbGVmdDogMHB4O1xuICAgIHRvcDogLThweDtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICBoZWlnaHQ6IDJweDtcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiBsaW5lYXItZ3JhZGllbnQoXG4gICAgICA5MGRlZyxcbiAgICAgIHJnYmEoMjI3LCAyNDIsIDI1MywgMSkgMCUsXG4gICAgICByZ2JhKDEwMCwgMTgxLCAyNDYsIDEpIDQ1JSxcbiAgICAgIHJnYmEoMTAwLCAxODEsIDI0NiwgMSkgNDUlXG4gICAgKTtcbiAgICBhbmltYXRpb246IG1vdmUgMnMgbGluZWFyIGluZmluaXRlO1xuICB9XG4gIEBrZXlmcmFtZXMgbW92ZSB7XG4gICAgMCUge1xuICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMCAwO1xuICAgIH1cbiAgICAxMDAlIHtcbiAgICAgIGJhY2tncm91bmQtcG9zaXRpb246IDI1MHB4IDI1MHB4O1xuICAgIH1cbiAgfVxuICAuaW5wdXQge1xuICAgICAgZm9udC1zaXplOiAxMDBweDtcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgIGxlZnQ6IDA7XG4gICAgICB0b3A6IDA7XG4gICAgICBvcGFjaXR5OiAwO1xuICB9XG4gIC5wcm9ncmVzcyB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHRvcDogLTdweDtcbiAgICBoZWlnaHQ6IDJweDtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICB3aWR0aDogY2FsYyggMTAwJSAtIDEwcHggKTtcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XG4gICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICBiYWNrZ3JvdW5kLWNsaXA6IHBhZGRpbmctYm94O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgbWFyZ2luLWxlZnQ6IDVweDsgXG4gIH1cbiAgLnByb2dyZXNzIC5pbmRldGVybWluYXRlIHtcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICMyMTk2RjM7IFxuICB9XG4gIC5wcm9ncmVzcyAuaW5kZXRlcm1pbmF0ZTpiZWZvcmUge1xuICAgIGNvbnRlbnQ6ICcnO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBpbmhlcml0O1xuICAgIHRvcDogMDtcbiAgICBsZWZ0OiAwO1xuICAgIGJvdHRvbTogMDtcbiAgICB3aWxsLWNoYW5nZTogbGVmdCwgcmlnaHQ7XG4gICAgLXdlYmtpdC1hbmltYXRpb246IGluZGV0ZXJtaW5hdGUgMi4xcyBjdWJpYy1iZXppZXIoMC42NSwgMC44MTUsIDAuNzM1LCAwLjM5NSkgaW5maW5pdGU7XG4gICAgICAgICAgICBhbmltYXRpb246IGluZGV0ZXJtaW5hdGUgMi4xcyBjdWJpYy1iZXppZXIoMC42NSwgMC44MTUsIDAuNzM1LCAwLjM5NSkgaW5maW5pdGU7IH1cbi5wcm9ncmVzcyAuaW5kZXRlcm1pbmF0ZTphZnRlciB7XG4gICAgY29udGVudDogJyc7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQtY29sb3I6IGluaGVyaXQ7XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgYm90dG9tOiAwO1xuICAgIHdpbGwtY2hhbmdlOiBsZWZ0LCByaWdodDtcbiAgICAtd2Via2l0LWFuaW1hdGlvbjogaW5kZXRlcm1pbmF0ZS1zaG9ydCAyLjFzIGN1YmljLWJlemllcigwLjE2NSwgMC44NCwgMC40NCwgMSkgaW5maW5pdGU7XG4gICAgICAgICAgICBhbmltYXRpb246IGluZGV0ZXJtaW5hdGUtc2hvcnQgMi4xcyBjdWJpYy1iZXppZXIoMC4xNjUsIDAuODQsIDAuNDQsIDEpIGluZmluaXRlO1xuICAgIC13ZWJraXQtYW5pbWF0aW9uLWRlbGF5OiAxLjE1cztcbiAgICAgICAgICAgIGFuaW1hdGlvbi1kZWxheTogMS4xNXM7IH1cbiAgICAgICAgICAgIEAtd2Via2l0LWtleWZyYW1lcyBpbmRldGVybWluYXRlIHtcbiAgICAgICAgICAgICAgMCUge1xuICAgICAgICAgICAgICAgIGxlZnQ6IC0zNSU7XG4gICAgICAgICAgICAgICAgcmlnaHQ6IDEwMCU7IH1cbiAgICAgICAgICAgICAgNjAlIHtcbiAgICAgICAgICAgICAgICBsZWZ0OiAxMDAlO1xuICAgICAgICAgICAgICAgIHJpZ2h0OiAtOTAlOyB9XG4gICAgICAgICAgICAgIDEwMCUge1xuICAgICAgICAgICAgICAgIGxlZnQ6IDEwMCU7XG4gICAgICAgICAgICAgICAgcmlnaHQ6IC05MCU7IH0gfVxuICAgICAgICAgICAgQGtleWZyYW1lcyBpbmRldGVybWluYXRlIHtcbiAgICAgICAgICAgICAgMCUge1xuICAgICAgICAgICAgICAgIGxlZnQ6IC0zNSU7XG4gICAgICAgICAgICAgICAgcmlnaHQ6IDEwMCU7IH1cbiAgICAgICAgICAgICAgNjAlIHtcbiAgICAgICAgICAgICAgICBsZWZ0OiAxMDAlO1xuICAgICAgICAgICAgICAgIHJpZ2h0OiAtOTAlOyB9XG4gICAgICAgICAgICAgIDEwMCUge1xuICAgICAgICAgICAgICAgIGxlZnQ6IDEwMCU7XG4gICAgICAgICAgICAgICAgcmlnaHQ6IC05MCU7IH0gfVxuICAgICAgICAgICAgQC13ZWJraXQta2V5ZnJhbWVzIGluZGV0ZXJtaW5hdGUtc2hvcnQge1xuICAgICAgICAgICAgICAwJSB7XG4gICAgICAgICAgICAgICAgbGVmdDogLTIwMCU7XG4gICAgICAgICAgICAgICAgcmlnaHQ6IDEwMCU7IH1cbiAgICAgICAgICAgICAgNjAlIHtcbiAgICAgICAgICAgICAgICBsZWZ0OiAxMDclO1xuICAgICAgICAgICAgICAgIHJpZ2h0OiAtOCU7IH1cbiAgICAgICAgICAgICAgMTAwJSB7XG4gICAgICAgICAgICAgICAgbGVmdDogMTA3JTtcbiAgICAgICAgICAgICAgICByaWdodDogLTglOyB9IH1cbiAgICAgICAgICAgIEBrZXlmcmFtZXMgaW5kZXRlcm1pbmF0ZS1zaG9ydCB7XG4gICAgICAgICAgICAgIDAlIHtcbiAgICAgICAgICAgICAgICBsZWZ0OiAtMjAwJTtcbiAgICAgICAgICAgICAgICByaWdodDogMTAwJTsgfVxuICAgICAgICAgICAgICA2MCUge1xuICAgICAgICAgICAgICAgIGxlZnQ6IDEwNyU7XG4gICAgICAgICAgICAgICAgcmlnaHQ6IC04JTsgfVxuICAgICAgICAgICAgICAxMDAlIHtcbiAgICAgICAgICAgICAgICBsZWZ0OiAxMDclO1xuICAgICAgICAgICAgICAgIHJpZ2h0OiAtOCU7IH0gfVxuICBgXG4gIF1cbn0pXG5leHBvcnQgY2xhc3MgRG9jVXBsb2FkTGliQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBpbnB1dFdpZHRoOiBudW1iZXI7XG5cbiAgQFZpZXdDaGlsZCgncHJvZ3Jlc3NiYXInLCB7IHN0YXRpYzogZmFsc2UgfSkgZWxlbWVudFByb2dyZXNzOiBFbGVtZW50UmVmO1xuICBAVmlld0NoaWxkKCdkb2N3cmFwcGVyJywgeyBzdGF0aWM6IGZhbHNlIH0pIGRvY1dyYXBwZXI6IEVsZW1lbnRSZWY7XG5cbiAgQElucHV0KClcbiAgaXNVcGxvYWROZWVkZWQ6IGJvb2xlYW4gPSB0cnVlO1xuICBASW5wdXQoKVxuICBpc0RlbGV0ZUFsbG93ZWQ6IGJvb2xlYW4gPSB0cnVlO1xuICBASW5wdXQoKVxuICBpc0RvY1ZlcmlmaWVkOiBib29sZWFuID0gZmFsc2U7XG4gIEBJbnB1dCgpXG4gIGZpbGVOYW1lOiBzdHJpbmc7XG4gIEBJbnB1dCgpXG4gIGZpbGVVcGxvYWRTdGF0dXM6IHN0cmluZztcbiAgQElucHV0KClcbiAgZmlsZURvd25sb2FkU3RyaW5nOiBzdHJpbmc7XG4gIEBJbnB1dCgpXG4gIGFjY2VwdDogc3RyaW5nO1xuXG4gIEBPdXRwdXQoKVxuICBlbWl0RmlsZTogRXZlbnRFbWl0dGVyPEZpbGU+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIEBPdXRwdXQoKVxuICBlbWl0RG9jVXJsOiBFdmVudEVtaXR0ZXI8c3RyaW5nPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gIG5nT25Jbml0KCk6IHZvaWQge1xuICB9XG5cbiAgbmdBZnRlclZpZXdJbml0KCkge1xuICAgIHRoaXMuaW5wdXRXaWR0aCA9IHRoaXMuZG9jV3JhcHBlci5uYXRpdmVFbGVtZW50LmlubmVyV2lkdGg7XG4gIH1cblxuXG4gIHVwbG9hZEZpbGUoZXZlbnQ6IGFueSkge1xuICAgIHRoaXMuZW1pdEZpbGUuZW1pdChldmVudC50YXJnZXQuZmlsZXNbMF0pO1xuICB9XG5cbiAgb25Eb3dubG9hZCgpIHtcbiAgICB0aGlzLmVtaXREb2NVcmwuZW1pdCh0aGlzLmZpbGVEb3dubG9hZFN0cmluZyk7XG4gIH1cblxufVxuIiwiPGRpdiBjbGFzcz1cInVwbG9hZC1idG4td3JhcHBlclwiPlxuICAgIDxkaXYgY2xhc3M9XCJ1cGxvYWQtYnRuXCIgI2RvY3dyYXBwZXI+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJmaWxlLWljb25cIiAoY2xpY2spPSd0aGlzLmZpbGUuY2xpY2soKSc+XG4gICAgICAgICAgICA8IS0tVE9ETzogcmVuZGVyIGljb24gYmFzZWQgb24gdXBsb2FkZWQgZmlsZSBleHRlbnNpb24tLT5cbiAgICAgICAgICAgIDxzcGFuPlxuICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnMtb3V0bGluZWRcIiBzdHlsZT1cImNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuNTQpO1wiPlxuICAgICAgICAgICAgICAgICAgICBpbnNlcnRfcGhvdG9cbiAgICAgICAgICAgICAgICA8L2k+IFxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzcz1cImZpbGUtbmFtZVwiIChjbGljayk9J3RoaXMuZmlsZS5jbGljaygpJz5cbiAgICAgICAgICAgIHt7ZmlsZU5hbWV9fVxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGRpdiBjbGFzcz1cImJ0blwiIFtuZ0NsYXNzXT1cImZpbGVVcGxvYWRTdGF0dXNcIj5cbiAgICAgICAgICAgIDxzcGFuICpuZ0lmPVwiaXNVcGxvYWROZWVkZWRcIiAoY2xpY2spPSd0aGlzLmZpbGUuY2xpY2soKSc+XG4gICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJtYXRlcmlhbC1pY29ucy1vdXRsaW5lZFwiIHN0eWxlPVwiY29sb3I6ICMyMTk2RjM7XCI+Y2xvdWRfdXBsb2FkPC9pPlxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgPHNwYW4gKm5nSWY9XCJmaWxlRG93bmxvYWRTdHJpbmdcIj5cbiAgICAgICAgICAgICAgICA8aSBjbGFzcz1cIm1hdGVyaWFsLWljb25zLW91dGxpbmVkXCIgc3R5bGU9XCJjb2xvcjogIzIxOTZGMztcIj5jbG91ZF9kb3dubG9hZDwvaT5cbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDxzcGFuICpuZ0lmPVwiZmlsZVVwbG9hZFN0YXR1cyA9PT0gJ3VwbG9hZGluZydcIj5cbiAgICAgICAgICAgICAgICA8aSBjbGFzcz1cIm1hdGVyaWFsLWljb25zXCIgc3R5bGU9XCJjb2xvcjogI0U2NTEwMDtcIj5jYW5jZWw8L2k+XG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICA8c3BhbiAqbmdJZj1cImZpbGVEb3dubG9hZFN0cmluZyAmJiBpc0RlbGV0ZUFsbG93ZWRcIj5cbiAgICAgICAgICAgICAgICA8aSBjbGFzcz1cIm1hdGVyaWFsLWljb25zXCIgc3R5bGU9XCJjb2xvcjogI0U2NTEwMDtcIj5kZWxldGU8L2k+XG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICA8c3BhbiAqbmdJZj1cImZpbGVEb3dubG9hZFN0cmluZyAmJiBpc0RvY1ZlcmlmaWVkXCI+XG4gICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJtYXRlcmlhbC1pY29uc1wiIHN0eWxlPVwiY29sb3I6ICMyNUJEMTc7XCI+Y2hlY2tfY2lyY2xlPC9pPlxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPGlucHV0IHR5cGU9XCJmaWxlXCIgY2xhc3M9XCJpbnB1dFwiICBuYW1lPVwiZmllbGROYW1lXCIgW2Rpc2FibGVkXT1cIiFpc1VwbG9hZE5lZWRlZFwiIFthY2NlcHRdPVwiYWNjZXB0XCIgKGNoYW5nZSk9XCJ1cGxvYWRGaWxlKCRldmVudClcIlxuICAgICAgICAgICAgI2ZpbGUgLz5cbiAgICA8L2Rpdj5cbiAgICA8ZGl2IGNsYXNzPVwicHJvZ3Jlc3NcIiAqbmdJZj1cImZpbGVVcGxvYWRTdGF0dXMgPT09ICd1cGxvYWRpbmcnXCI+XG4gICAgICAgIDxkaXYgY2xhc3M9XCJpbmRldGVybWluYXRlXCI+PC9kaXY+XG4gICAgPC9kaXY+XG48L2Rpdj4iXX0=