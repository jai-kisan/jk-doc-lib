import { NgModule } from '@angular/core';
import { DocUploadLibComponent } from './doc-upload-lib.component';
import { CommonModule } from '@angular/common';
import * as i0 from "@angular/core";
export class DocUploadLibModule {
}
DocUploadLibModule.ɵmod = i0.ɵɵdefineNgModule({ type: DocUploadLibModule });
DocUploadLibModule.ɵinj = i0.ɵɵdefineInjector({ factory: function DocUploadLibModule_Factory(t) { return new (t || DocUploadLibModule)(); }, imports: [[
            CommonModule
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(DocUploadLibModule, { declarations: [DocUploadLibComponent], imports: [CommonModule], exports: [DocUploadLibComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(DocUploadLibModule, [{
        type: NgModule,
        args: [{
                declarations: [DocUploadLibComponent],
                imports: [
                    CommonModule
                ],
                exports: [DocUploadLibComponent]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG9jLXVwbG9hZC1saWIubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZG9jLXVwbG9hZC1saWIvIiwic291cmNlcyI6WyJsaWIvZG9jLXVwbG9hZC1saWIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDbkUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDOztBQVcvQyxNQUFNLE9BQU8sa0JBQWtCOztzREFBbEIsa0JBQWtCO21IQUFsQixrQkFBa0Isa0JBTHBCO1lBQ1AsWUFBWTtTQUNiO3dGQUdVLGtCQUFrQixtQkFOZCxxQkFBcUIsYUFFbEMsWUFBWSxhQUVKLHFCQUFxQjtrREFFcEIsa0JBQWtCO2NBUDlCLFFBQVE7ZUFBQztnQkFDUixZQUFZLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQztnQkFDckMsT0FBTyxFQUFFO29CQUNQLFlBQVk7aUJBQ2I7Z0JBQ0QsT0FBTyxFQUFFLENBQUMscUJBQXFCLENBQUM7YUFDakMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRG9jVXBsb2FkTGliQ29tcG9uZW50IH0gZnJvbSAnLi9kb2MtdXBsb2FkLWxpYi5jb21wb25lbnQnO1xuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcblxuXG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW0RvY1VwbG9hZExpYkNvbXBvbmVudF0sXG4gIGltcG9ydHM6IFtcbiAgICBDb21tb25Nb2R1bGVcbiAgXSxcbiAgZXhwb3J0czogW0RvY1VwbG9hZExpYkNvbXBvbmVudF1cbn0pXG5leHBvcnQgY2xhc3MgRG9jVXBsb2FkTGliTW9kdWxlIHsgfVxuIl19