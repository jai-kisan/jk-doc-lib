import { Component, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
var _c0 = ["progressbar"];
var _c1 = ["docwrapper"];
function DocUploadLibComponent_span_10_Template(rf, ctx) { if (rf & 1) {
    var _r9 = i0.ɵɵgetCurrentView();
    i0.ɵɵelementStart(0, "span", 12);
    i0.ɵɵlistener("click", function DocUploadLibComponent_span_10_Template_span_click_0_listener() { i0.ɵɵrestoreView(_r9); i0.ɵɵnextContext(); var _r6 = i0.ɵɵreference(16); return _r6.click(); });
    i0.ɵɵelementStart(1, "i", 13);
    i0.ɵɵtext(2, "cloud_upload");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function DocUploadLibComponent_span_11_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "i", 13);
    i0.ɵɵtext(2, "cloud_download");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function DocUploadLibComponent_span_12_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "i", 14);
    i0.ɵɵtext(2, "cancel");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function DocUploadLibComponent_span_13_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "i", 14);
    i0.ɵɵtext(2, "delete");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function DocUploadLibComponent_span_14_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "span");
    i0.ɵɵelementStart(1, "i", 15);
    i0.ɵɵtext(2, "check_circle");
    i0.ɵɵelementEnd();
    i0.ɵɵelementEnd();
} }
function DocUploadLibComponent_div_17_Template(rf, ctx) { if (rf & 1) {
    i0.ɵɵelementStart(0, "div", 16);
    i0.ɵɵelement(1, "div", 17);
    i0.ɵɵelementEnd();
} }
var DocUploadLibComponent = /** @class */ (function () {
    function DocUploadLibComponent() {
        this.isUploadNeeded = true;
        this.isDeleteAllowed = true;
        this.isDocVerified = false;
        this.emitFile = new EventEmitter();
        this.emitDocUrl = new EventEmitter();
    }
    DocUploadLibComponent.prototype.ngOnInit = function () {
    };
    DocUploadLibComponent.prototype.ngAfterViewInit = function () {
        this.inputWidth = this.docWrapper.nativeElement.innerWidth;
    };
    DocUploadLibComponent.prototype.uploadFile = function (event) {
        this.emitFile.emit(event.target.files[0]);
    };
    DocUploadLibComponent.prototype.onDownload = function () {
        this.emitDocUrl.emit(this.fileDownloadString);
    };
    DocUploadLibComponent.ɵfac = function DocUploadLibComponent_Factory(t) { return new (t || DocUploadLibComponent)(); };
    DocUploadLibComponent.ɵcmp = i0.ɵɵdefineComponent({ type: DocUploadLibComponent, selectors: [["doc-doc-upload-lib"]], viewQuery: function DocUploadLibComponent_Query(rf, ctx) { if (rf & 1) {
            i0.ɵɵviewQuery(_c0, true);
            i0.ɵɵviewQuery(_c1, true);
        } if (rf & 2) {
            var _t;
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.elementProgress = _t.first);
            i0.ɵɵqueryRefresh(_t = i0.ɵɵloadQuery()) && (ctx.docWrapper = _t.first);
        } }, inputs: { isUploadNeeded: "isUploadNeeded", isDeleteAllowed: "isDeleteAllowed", isDocVerified: "isDocVerified", fileName: "fileName", fileUploadStatus: "fileUploadStatus", fileDownloadString: "fileDownloadString", accept: "accept" }, outputs: { emitFile: "emitFile", emitDocUrl: "emitDocUrl" }, decls: 18, vars: 10, consts: [[1, "upload-btn-wrapper"], [1, "upload-btn"], ["docwrapper", ""], [1, "file-icon", 3, "click"], [1, "material-icons-outlined", 2, "color", "rgba(0, 0, 0, 0.54)"], [1, "file-name", 3, "click"], [1, "btn", 3, "ngClass"], [3, "click", 4, "ngIf"], [4, "ngIf"], ["type", "file", "name", "fieldName", 1, "input", 3, "disabled", "accept", "change"], ["file", ""], ["class", "progress", 4, "ngIf"], [3, "click"], [1, "material-icons-outlined", 2, "color", "#2196F3"], [1, "material-icons", 2, "color", "#E65100"], [1, "material-icons", 2, "color", "#25BD17"], [1, "progress"], [1, "indeterminate"]], template: function DocUploadLibComponent_Template(rf, ctx) { if (rf & 1) {
            var _r10 = i0.ɵɵgetCurrentView();
            i0.ɵɵelementStart(0, "div", 0);
            i0.ɵɵelementStart(1, "div", 1, 2);
            i0.ɵɵelementStart(3, "div", 3);
            i0.ɵɵlistener("click", function DocUploadLibComponent_Template_div_click_3_listener() { i0.ɵɵrestoreView(_r10); var _r6 = i0.ɵɵreference(16); return _r6.click(); });
            i0.ɵɵelementStart(4, "span");
            i0.ɵɵelementStart(5, "i", 4);
            i0.ɵɵtext(6, " insert_photo ");
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(7, "div", 5);
            i0.ɵɵlistener("click", function DocUploadLibComponent_Template_div_click_7_listener() { i0.ɵɵrestoreView(_r10); var _r6 = i0.ɵɵreference(16); return _r6.click(); });
            i0.ɵɵtext(8);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(9, "div", 6);
            i0.ɵɵtemplate(10, DocUploadLibComponent_span_10_Template, 3, 0, "span", 7);
            i0.ɵɵtemplate(11, DocUploadLibComponent_span_11_Template, 3, 0, "span", 8);
            i0.ɵɵtemplate(12, DocUploadLibComponent_span_12_Template, 3, 0, "span", 8);
            i0.ɵɵtemplate(13, DocUploadLibComponent_span_13_Template, 3, 0, "span", 8);
            i0.ɵɵtemplate(14, DocUploadLibComponent_span_14_Template, 3, 0, "span", 8);
            i0.ɵɵelementEnd();
            i0.ɵɵelementStart(15, "input", 9, 10);
            i0.ɵɵlistener("change", function DocUploadLibComponent_Template_input_change_15_listener($event) { return ctx.uploadFile($event); });
            i0.ɵɵelementEnd();
            i0.ɵɵelementEnd();
            i0.ɵɵtemplate(17, DocUploadLibComponent_div_17_Template, 2, 0, "div", 11);
            i0.ɵɵelementEnd();
        } if (rf & 2) {
            i0.ɵɵadvance(8);
            i0.ɵɵtextInterpolate1(" ", ctx.fileName, " ");
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngClass", ctx.fileUploadStatus);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.isUploadNeeded);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.fileDownloadString);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.fileUploadStatus === "uploading");
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.fileDownloadString && ctx.isDeleteAllowed);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("ngIf", ctx.fileDownloadString && ctx.isDocVerified);
            i0.ɵɵadvance(1);
            i0.ɵɵproperty("disabled", !ctx.isUploadNeeded)("accept", ctx.accept);
            i0.ɵɵadvance(2);
            i0.ɵɵproperty("ngIf", ctx.fileUploadStatus === "uploading");
        } }, directives: [i1.NgClass, i1.NgIf], styles: [".upload-btn-wrapper[_ngcontent-%COMP%]{\n      position: relative;\n      display: inline-block;\n    }\n    .upload-btn[_ngcontent-%COMP%] {\n      position: relative;\n      overflow: hidden;\n      display: inline-flex;\n      flex-direction: row;\n      margin: 5px;\n      cursor: pointer;\n      background: rgba(0, 0, 0, 0.04);\n      border-radius: 4px;\n      height: 40px;\n      padding: 7px;\n      box-sizing: border-box;\n      background: #E3F2FD;\n    }\n    .file-icon[_ngcontent-%COMP%] {\n      margin-right: 5px;\n      align-self: center;\n      z-index: 1;\n    }\n    .file-icon[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n      font-size: 26px;\n      height: 26px;\n      width: 26px;\n    }\n    .file-name[_ngcontent-%COMP%] {\n      margin-right: 5px;\n      text-overflow: ellipsis;\n      align-self: center;\n      flex: 1;\n      box-sizing: border-box;\n      padding-bottom: 4px;\n      font-size: 12px;\n      line-height: 20px;\n      letter-spacing: 0.25px;\n      color: #828282;\n      mix-blend-mode: normal;\n      z-index: 1;\n    }\n  .upload-btn[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%] {\n    align-self: center;\n      margin-left: 5px;\n      color: #2196f3;\n      z-index: 1;\n  }\n  .upload-btn[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{\n    margin: 5px;\n    z-index: 1;\n  }\n  \n  .upload-anim[_ngcontent-%COMP%]{\n    height: 2px;\n    position: relative;\n    left: 0px;\n    top: -8px;\n    border-radius: 4px;\n    box-sizing: border-box;\n    height: 2px;\n    background-image: linear-gradient(\n      90deg,\n      rgba(227, 242, 253, 1) 0%,\n      rgba(100, 181, 246, 1) 45%,\n      rgba(100, 181, 246, 1) 45%\n    );\n    animation: move 2s linear infinite;\n  }\n  @keyframes move {\n    0% {\n      background-position: 0 0;\n    }\n    100% {\n      background-position: 250px 250px;\n    }\n  }\n  .input[_ngcontent-%COMP%] {\n      font-size: 100px;\n      position: absolute;\n      left: 0;\n      top: 0;\n      opacity: 0;\n  }\n  .progress[_ngcontent-%COMP%] {\n    position: relative;\n    top: -7px;\n    height: 2px;\n    display: block;\n    width: calc( 100% - 10px );\n    border-radius: 2px;\n    box-sizing: border-box;\n    background-clip: padding-box;\n    overflow: hidden;\n    margin-left: 5px; \n  }\n  .progress[_ngcontent-%COMP%]   .indeterminate[_ngcontent-%COMP%] {\n      background-color: #2196F3; \n  }\n  .progress[_ngcontent-%COMP%]   .indeterminate[_ngcontent-%COMP%]:before {\n    content: '';\n    position: absolute;\n    background-color: inherit;\n    top: 0;\n    left: 0;\n    bottom: 0;\n    will-change: left, right;\n    -webkit-animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite;\n            animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite; }\n.progress[_ngcontent-%COMP%]   .indeterminate[_ngcontent-%COMP%]:after {\n    content: '';\n    position: absolute;\n    background-color: inherit;\n    top: 0;\n    left: 0;\n    bottom: 0;\n    will-change: left, right;\n    -webkit-animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n            animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n    -webkit-animation-delay: 1.15s;\n            animation-delay: 1.15s; }\n            @-webkit-keyframes indeterminate {\n              0% {\n                left: -35%;\n                right: 100%; }\n              60% {\n                left: 100%;\n                right: -90%; }\n              100% {\n                left: 100%;\n                right: -90%; } }\n            @keyframes indeterminate {\n              0% {\n                left: -35%;\n                right: 100%; }\n              60% {\n                left: 100%;\n                right: -90%; }\n              100% {\n                left: 100%;\n                right: -90%; } }\n            @-webkit-keyframes indeterminate-short {\n              0% {\n                left: -200%;\n                right: 100%; }\n              60% {\n                left: 107%;\n                right: -8%; }\n              100% {\n                left: 107%;\n                right: -8%; } }\n            @keyframes indeterminate-short {\n              0% {\n                left: -200%;\n                right: 100%; }\n              60% {\n                left: 107%;\n                right: -8%; }\n              100% {\n                left: 107%;\n                right: -8%; } }"] });
    return DocUploadLibComponent;
}());
export { DocUploadLibComponent };
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(DocUploadLibComponent, [{
        type: Component,
        args: [{
                selector: 'doc-doc-upload-lib',
                templateUrl: './doc-upload-lib.component.html',
                styles: [
                    ".upload-btn-wrapper{\n      position: relative;\n      display: inline-block;\n    }\n    .upload-btn {\n      position: relative;\n      overflow: hidden;\n      display: inline-flex;\n      flex-direction: row;\n      margin: 5px;\n      cursor: pointer;\n      background: rgba(0, 0, 0, 0.04);\n      border-radius: 4px;\n      height: 40px;\n      padding: 7px;\n      box-sizing: border-box;\n      background: #E3F2FD;\n    }\n    .file-icon {\n      margin-right: 5px;\n      align-self: center;\n      z-index: 1;\n    }\n    .file-icon i {\n      font-size: 26px;\n      height: 26px;\n      width: 26px;\n    }\n    .file-name {\n      margin-right: 5px;\n      text-overflow: ellipsis;\n      align-self: center;\n      flex: 1;\n      box-sizing: border-box;\n      padding-bottom: 4px;\n      font-size: 12px;\n      line-height: 20px;\n      letter-spacing: 0.25px;\n      color: #828282;\n      mix-blend-mode: normal;\n      z-index: 1;\n    }\n  .upload-btn .btn {\n    align-self: center;\n      margin-left: 5px;\n      color: #2196f3;\n      z-index: 1;\n  }\n  .upload-btn .btn span{\n    margin: 5px;\n    z-index: 1;\n  }\n  \n  .upload-anim{\n    height: 2px;\n    position: relative;\n    left: 0px;\n    top: -8px;\n    border-radius: 4px;\n    box-sizing: border-box;\n    height: 2px;\n    background-image: linear-gradient(\n      90deg,\n      rgba(227, 242, 253, 1) 0%,\n      rgba(100, 181, 246, 1) 45%,\n      rgba(100, 181, 246, 1) 45%\n    );\n    animation: move 2s linear infinite;\n  }\n  @keyframes move {\n    0% {\n      background-position: 0 0;\n    }\n    100% {\n      background-position: 250px 250px;\n    }\n  }\n  .input {\n      font-size: 100px;\n      position: absolute;\n      left: 0;\n      top: 0;\n      opacity: 0;\n  }\n  .progress {\n    position: relative;\n    top: -7px;\n    height: 2px;\n    display: block;\n    width: calc( 100% - 10px );\n    border-radius: 2px;\n    box-sizing: border-box;\n    background-clip: padding-box;\n    overflow: hidden;\n    margin-left: 5px; \n  }\n  .progress .indeterminate {\n      background-color: #2196F3; \n  }\n  .progress .indeterminate:before {\n    content: '';\n    position: absolute;\n    background-color: inherit;\n    top: 0;\n    left: 0;\n    bottom: 0;\n    will-change: left, right;\n    -webkit-animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite;\n            animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite; }\n.progress .indeterminate:after {\n    content: '';\n    position: absolute;\n    background-color: inherit;\n    top: 0;\n    left: 0;\n    bottom: 0;\n    will-change: left, right;\n    -webkit-animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n            animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n    -webkit-animation-delay: 1.15s;\n            animation-delay: 1.15s; }\n            @-webkit-keyframes indeterminate {\n              0% {\n                left: -35%;\n                right: 100%; }\n              60% {\n                left: 100%;\n                right: -90%; }\n              100% {\n                left: 100%;\n                right: -90%; } }\n            @keyframes indeterminate {\n              0% {\n                left: -35%;\n                right: 100%; }\n              60% {\n                left: 100%;\n                right: -90%; }\n              100% {\n                left: 100%;\n                right: -90%; } }\n            @-webkit-keyframes indeterminate-short {\n              0% {\n                left: -200%;\n                right: 100%; }\n              60% {\n                left: 107%;\n                right: -8%; }\n              100% {\n                left: 107%;\n                right: -8%; } }\n            @keyframes indeterminate-short {\n              0% {\n                left: -200%;\n                right: 100%; }\n              60% {\n                left: 107%;\n                right: -8%; }\n              100% {\n                left: 107%;\n                right: -8%; } }\n  "
                ]
            }]
    }], function () { return []; }, { elementProgress: [{
            type: ViewChild,
            args: ['progressbar', { static: false }]
        }], docWrapper: [{
            type: ViewChild,
            args: ['docwrapper', { static: false }]
        }], isUploadNeeded: [{
            type: Input
        }], isDeleteAllowed: [{
            type: Input
        }], isDocVerified: [{
            type: Input
        }], fileName: [{
            type: Input
        }], fileUploadStatus: [{
            type: Input
        }], fileDownloadString: [{
            type: Input
        }], accept: [{
            type: Input
        }], emitFile: [{
            type: Output
        }], emitDocUrl: [{
            type: Output
        }] }); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG9jLXVwbG9hZC1saWIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vZG9jLXVwbG9hZC1saWIvIiwic291cmNlcyI6WyJsaWIvZG9jLXVwbG9hZC1saWIuY29tcG9uZW50LnRzIiwibGliL2RvYy11cGxvYWQtbGliLmNvbXBvbmVudC5odG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsU0FBUyxFQUFjLE1BQU0sRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7Ozs7O0lDYzFGLGdDQUNJO0lBRHlCLGlMQUFTLFdBQWlCLElBQUM7SUFDcEQsNkJBQTJEO0lBQUEsNEJBQVk7SUFBQSxpQkFBSTtJQUMvRSxpQkFBTzs7O0lBQ1AsNEJBQ0k7SUFBQSw2QkFBMkQ7SUFBQSw4QkFBYztJQUFBLGlCQUFJO0lBQ2pGLGlCQUFPOzs7SUFDUCw0QkFDSTtJQUFBLDZCQUFrRDtJQUFBLHNCQUFNO0lBQUEsaUJBQUk7SUFDaEUsaUJBQU87OztJQUNQLDRCQUNJO0lBQUEsNkJBQWtEO0lBQUEsc0JBQU07SUFBQSxpQkFBSTtJQUNoRSxpQkFBTzs7O0lBQ1AsNEJBQ0k7SUFBQSw2QkFBa0Q7SUFBQSw0QkFBWTtJQUFBLGlCQUFJO0lBQ3RFLGlCQUFPOzs7SUFLZiwrQkFDSTtJQUFBLDBCQUFpQztJQUNyQyxpQkFBTTs7QURqQ1Y7SUFvTUU7UUFwQkEsbUJBQWMsR0FBWSxJQUFJLENBQUM7UUFFL0Isb0JBQWUsR0FBWSxJQUFJLENBQUM7UUFFaEMsa0JBQWEsR0FBWSxLQUFLLENBQUM7UUFXL0IsYUFBUSxHQUF1QixJQUFJLFlBQVksRUFBRSxDQUFDO1FBR2xELGVBQVUsR0FBeUIsSUFBSSxZQUFZLEVBQUUsQ0FBQztJQUV0QyxDQUFDO0lBRWpCLHdDQUFRLEdBQVI7SUFDQSxDQUFDO0lBRUQsK0NBQWUsR0FBZjtRQUNFLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsVUFBVSxDQUFDO0lBQzdELENBQUM7SUFHRCwwQ0FBVSxHQUFWLFVBQVcsS0FBVTtRQUNuQixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzVDLENBQUM7SUFFRCwwQ0FBVSxHQUFWO1FBQ0UsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7SUFDaEQsQ0FBQzs4RkE1Q1UscUJBQXFCOzhEQUFyQixxQkFBcUI7Ozs7Ozs7OztZQzFLbEMsOEJBQ0k7WUFBQSxpQ0FDSTtZQUFBLDhCQUNJO1lBRG1CLHFKQUFTLFdBQWlCLElBQUM7WUFFOUMsNEJBQ0k7WUFBQSw0QkFDSTtZQUFBLDhCQUNKO1lBQUEsaUJBQUk7WUFDUixpQkFBTztZQUNYLGlCQUFNO1lBQ04sOEJBQ0k7WUFEbUIscUpBQVMsV0FBaUIsSUFBQztZQUM5QyxZQUNKO1lBQUEsaUJBQU07WUFDTiw4QkFDSTtZQUFBLDBFQUNJO1lBRUosMEVBQ0k7WUFFSiwwRUFDSTtZQUVKLDBFQUNJO1lBRUosMEVBQ0k7WUFFUixpQkFBTTtZQUNOLHFDQUVKO1lBRnNHLDBHQUFVLHNCQUFrQixJQUFDO1lBQS9ILGlCQUVKO1lBQUEsaUJBQU07WUFDTix5RUFDSTtZQUVSLGlCQUFNOztZQXpCTSxlQUNKO1lBREksNkNBQ0o7WUFDaUIsZUFBNEI7WUFBNUIsOENBQTRCO1lBQ25DLGVBQXNCO1lBQXRCLHlDQUFzQjtZQUd0QixlQUEwQjtZQUExQiw2Q0FBMEI7WUFHMUIsZUFBd0M7WUFBeEMsMkRBQXdDO1lBR3hDLGVBQTZDO1lBQTdDLG9FQUE2QztZQUc3QyxlQUEyQztZQUEzQyxrRUFBMkM7WUFJRixlQUE0QjtZQUE1Qiw4Q0FBNEIsc0JBQUE7WUFHN0QsZUFBd0M7WUFBeEMsMkRBQXdDOztnQ0RqQ2xFO0NBd05DLEFBdE5ELElBc05DO1NBOUNZLHFCQUFxQjtrREFBckIscUJBQXFCO2NBeEtqQyxTQUFTO2VBQUM7Z0JBQ1QsUUFBUSxFQUFFLG9CQUFvQjtnQkFDOUIsV0FBVyxFQUFFLGlDQUFpQztnQkFDOUMsTUFBTSxFQUFFO29CQUNOLDBoSUFpS0Q7aUJBQ0E7YUFDRjs7a0JBS0UsU0FBUzttQkFBQyxhQUFhLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFOztrQkFDMUMsU0FBUzttQkFBQyxZQUFZLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFOztrQkFFekMsS0FBSzs7a0JBRUwsS0FBSzs7a0JBRUwsS0FBSzs7a0JBRUwsS0FBSzs7a0JBRUwsS0FBSzs7a0JBRUwsS0FBSzs7a0JBRUwsS0FBSzs7a0JBR0wsTUFBTTs7a0JBR04sTUFBTSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3Q2hpbGQsIEVsZW1lbnRSZWYsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdkb2MtZG9jLXVwbG9hZC1saWInLFxuICB0ZW1wbGF0ZVVybDogJy4vZG9jLXVwbG9hZC1saWIuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZXM6IFtcbiAgICBgLnVwbG9hZC1idG4td3JhcHBlcntcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB9XG4gICAgLnVwbG9hZC1idG4ge1xuICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgIG1hcmdpbjogNXB4O1xuICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgICAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjA0KTtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICAgIGhlaWdodDogNDBweDtcbiAgICAgIHBhZGRpbmc6IDdweDtcbiAgICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgICBiYWNrZ3JvdW5kOiAjRTNGMkZEO1xuICAgIH1cbiAgICAuZmlsZS1pY29uIHtcbiAgICAgIG1hcmdpbi1yaWdodDogNXB4O1xuICAgICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgICAgei1pbmRleDogMTtcbiAgICB9XG4gICAgLmZpbGUtaWNvbiBpIHtcbiAgICAgIGZvbnQtc2l6ZTogMjZweDtcbiAgICAgIGhlaWdodDogMjZweDtcbiAgICAgIHdpZHRoOiAyNnB4O1xuICAgIH1cbiAgICAuZmlsZS1uYW1lIHtcbiAgICAgIG1hcmdpbi1yaWdodDogNXB4O1xuICAgICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gICAgICBhbGlnbi1zZWxmOiBjZW50ZXI7XG4gICAgICBmbGV4OiAxO1xuICAgICAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICAgIHBhZGRpbmctYm90dG9tOiA0cHg7XG4gICAgICBmb250LXNpemU6IDEycHg7XG4gICAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICAgIGxldHRlci1zcGFjaW5nOiAwLjI1cHg7XG4gICAgICBjb2xvcjogIzgyODI4MjtcbiAgICAgIG1peC1ibGVuZC1tb2RlOiBub3JtYWw7XG4gICAgICB6LWluZGV4OiAxO1xuICAgIH1cbiAgLnVwbG9hZC1idG4gLmJ0biB7XG4gICAgYWxpZ24tc2VsZjogY2VudGVyO1xuICAgICAgbWFyZ2luLWxlZnQ6IDVweDtcbiAgICAgIGNvbG9yOiAjMjE5NmYzO1xuICAgICAgei1pbmRleDogMTtcbiAgfVxuICAudXBsb2FkLWJ0biAuYnRuIHNwYW57XG4gICAgbWFyZ2luOiA1cHg7XG4gICAgei1pbmRleDogMTtcbiAgfVxuICBcbiAgLnVwbG9hZC1hbmlte1xuICAgIGhlaWdodDogMnB4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBsZWZ0OiAwcHg7XG4gICAgdG9wOiAtOHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGhlaWdodDogMnB4O1xuICAgIGJhY2tncm91bmQtaW1hZ2U6IGxpbmVhci1ncmFkaWVudChcbiAgICAgIDkwZGVnLFxuICAgICAgcmdiYSgyMjcsIDI0MiwgMjUzLCAxKSAwJSxcbiAgICAgIHJnYmEoMTAwLCAxODEsIDI0NiwgMSkgNDUlLFxuICAgICAgcmdiYSgxMDAsIDE4MSwgMjQ2LCAxKSA0NSVcbiAgICApO1xuICAgIGFuaW1hdGlvbjogbW92ZSAycyBsaW5lYXIgaW5maW5pdGU7XG4gIH1cbiAgQGtleWZyYW1lcyBtb3ZlIHtcbiAgICAwJSB7XG4gICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiAwIDA7XG4gICAgfVxuICAgIDEwMCUge1xuICAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogMjUwcHggMjUwcHg7XG4gICAgfVxuICB9XG4gIC5pbnB1dCB7XG4gICAgICBmb250LXNpemU6IDEwMHB4O1xuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgbGVmdDogMDtcbiAgICAgIHRvcDogMDtcbiAgICAgIG9wYWNpdHk6IDA7XG4gIH1cbiAgLnByb2dyZXNzIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgdG9wOiAtN3B4O1xuICAgIGhlaWdodDogMnB4O1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHdpZHRoOiBjYWxjKCAxMDAlIC0gMTBweCApO1xuICAgIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGJhY2tncm91bmQtY2xpcDogcGFkZGluZy1ib3g7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBtYXJnaW4tbGVmdDogNXB4OyBcbiAgfVxuICAucHJvZ3Jlc3MgLmluZGV0ZXJtaW5hdGUge1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzIxOTZGMzsgXG4gIH1cbiAgLnByb2dyZXNzIC5pbmRldGVybWluYXRlOmJlZm9yZSB7XG4gICAgY29udGVudDogJyc7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGJhY2tncm91bmQtY29sb3I6IGluaGVyaXQ7XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgYm90dG9tOiAwO1xuICAgIHdpbGwtY2hhbmdlOiBsZWZ0LCByaWdodDtcbiAgICAtd2Via2l0LWFuaW1hdGlvbjogaW5kZXRlcm1pbmF0ZSAyLjFzIGN1YmljLWJlemllcigwLjY1LCAwLjgxNSwgMC43MzUsIDAuMzk1KSBpbmZpbml0ZTtcbiAgICAgICAgICAgIGFuaW1hdGlvbjogaW5kZXRlcm1pbmF0ZSAyLjFzIGN1YmljLWJlemllcigwLjY1LCAwLjgxNSwgMC43MzUsIDAuMzk1KSBpbmZpbml0ZTsgfVxuLnByb2dyZXNzIC5pbmRldGVybWluYXRlOmFmdGVyIHtcbiAgICBjb250ZW50OiAnJztcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogaW5oZXJpdDtcbiAgICB0b3A6IDA7XG4gICAgbGVmdDogMDtcbiAgICBib3R0b206IDA7XG4gICAgd2lsbC1jaGFuZ2U6IGxlZnQsIHJpZ2h0O1xuICAgIC13ZWJraXQtYW5pbWF0aW9uOiBpbmRldGVybWluYXRlLXNob3J0IDIuMXMgY3ViaWMtYmV6aWVyKDAuMTY1LCAwLjg0LCAwLjQ0LCAxKSBpbmZpbml0ZTtcbiAgICAgICAgICAgIGFuaW1hdGlvbjogaW5kZXRlcm1pbmF0ZS1zaG9ydCAyLjFzIGN1YmljLWJlemllcigwLjE2NSwgMC44NCwgMC40NCwgMSkgaW5maW5pdGU7XG4gICAgLXdlYmtpdC1hbmltYXRpb24tZGVsYXk6IDEuMTVzO1xuICAgICAgICAgICAgYW5pbWF0aW9uLWRlbGF5OiAxLjE1czsgfVxuICAgICAgICAgICAgQC13ZWJraXQta2V5ZnJhbWVzIGluZGV0ZXJtaW5hdGUge1xuICAgICAgICAgICAgICAwJSB7XG4gICAgICAgICAgICAgICAgbGVmdDogLTM1JTtcbiAgICAgICAgICAgICAgICByaWdodDogMTAwJTsgfVxuICAgICAgICAgICAgICA2MCUge1xuICAgICAgICAgICAgICAgIGxlZnQ6IDEwMCU7XG4gICAgICAgICAgICAgICAgcmlnaHQ6IC05MCU7IH1cbiAgICAgICAgICAgICAgMTAwJSB7XG4gICAgICAgICAgICAgICAgbGVmdDogMTAwJTtcbiAgICAgICAgICAgICAgICByaWdodDogLTkwJTsgfSB9XG4gICAgICAgICAgICBAa2V5ZnJhbWVzIGluZGV0ZXJtaW5hdGUge1xuICAgICAgICAgICAgICAwJSB7XG4gICAgICAgICAgICAgICAgbGVmdDogLTM1JTtcbiAgICAgICAgICAgICAgICByaWdodDogMTAwJTsgfVxuICAgICAgICAgICAgICA2MCUge1xuICAgICAgICAgICAgICAgIGxlZnQ6IDEwMCU7XG4gICAgICAgICAgICAgICAgcmlnaHQ6IC05MCU7IH1cbiAgICAgICAgICAgICAgMTAwJSB7XG4gICAgICAgICAgICAgICAgbGVmdDogMTAwJTtcbiAgICAgICAgICAgICAgICByaWdodDogLTkwJTsgfSB9XG4gICAgICAgICAgICBALXdlYmtpdC1rZXlmcmFtZXMgaW5kZXRlcm1pbmF0ZS1zaG9ydCB7XG4gICAgICAgICAgICAgIDAlIHtcbiAgICAgICAgICAgICAgICBsZWZ0OiAtMjAwJTtcbiAgICAgICAgICAgICAgICByaWdodDogMTAwJTsgfVxuICAgICAgICAgICAgICA2MCUge1xuICAgICAgICAgICAgICAgIGxlZnQ6IDEwNyU7XG4gICAgICAgICAgICAgICAgcmlnaHQ6IC04JTsgfVxuICAgICAgICAgICAgICAxMDAlIHtcbiAgICAgICAgICAgICAgICBsZWZ0OiAxMDclO1xuICAgICAgICAgICAgICAgIHJpZ2h0OiAtOCU7IH0gfVxuICAgICAgICAgICAgQGtleWZyYW1lcyBpbmRldGVybWluYXRlLXNob3J0IHtcbiAgICAgICAgICAgICAgMCUge1xuICAgICAgICAgICAgICAgIGxlZnQ6IC0yMDAlO1xuICAgICAgICAgICAgICAgIHJpZ2h0OiAxMDAlOyB9XG4gICAgICAgICAgICAgIDYwJSB7XG4gICAgICAgICAgICAgICAgbGVmdDogMTA3JTtcbiAgICAgICAgICAgICAgICByaWdodDogLTglOyB9XG4gICAgICAgICAgICAgIDEwMCUge1xuICAgICAgICAgICAgICAgIGxlZnQ6IDEwNyU7XG4gICAgICAgICAgICAgICAgcmlnaHQ6IC04JTsgfSB9XG4gIGBcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBEb2NVcGxvYWRMaWJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGlucHV0V2lkdGg6IG51bWJlcjtcblxuICBAVmlld0NoaWxkKCdwcm9ncmVzc2JhcicsIHsgc3RhdGljOiBmYWxzZSB9KSBlbGVtZW50UHJvZ3Jlc3M6IEVsZW1lbnRSZWY7XG4gIEBWaWV3Q2hpbGQoJ2RvY3dyYXBwZXInLCB7IHN0YXRpYzogZmFsc2UgfSkgZG9jV3JhcHBlcjogRWxlbWVudFJlZjtcblxuICBASW5wdXQoKVxuICBpc1VwbG9hZE5lZWRlZDogYm9vbGVhbiA9IHRydWU7XG4gIEBJbnB1dCgpXG4gIGlzRGVsZXRlQWxsb3dlZDogYm9vbGVhbiA9IHRydWU7XG4gIEBJbnB1dCgpXG4gIGlzRG9jVmVyaWZpZWQ6IGJvb2xlYW4gPSBmYWxzZTtcbiAgQElucHV0KClcbiAgZmlsZU5hbWU6IHN0cmluZztcbiAgQElucHV0KClcbiAgZmlsZVVwbG9hZFN0YXR1czogc3RyaW5nO1xuICBASW5wdXQoKVxuICBmaWxlRG93bmxvYWRTdHJpbmc6IHN0cmluZztcbiAgQElucHV0KClcbiAgYWNjZXB0OiBzdHJpbmc7XG5cbiAgQE91dHB1dCgpXG4gIGVtaXRGaWxlOiBFdmVudEVtaXR0ZXI8RmlsZT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgQE91dHB1dCgpXG4gIGVtaXREb2NVcmw6IEV2ZW50RW1pdHRlcjxzdHJpbmc+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKTogdm9pZCB7XG4gIH1cblxuICBuZ0FmdGVyVmlld0luaXQoKSB7XG4gICAgdGhpcy5pbnB1dFdpZHRoID0gdGhpcy5kb2NXcmFwcGVyLm5hdGl2ZUVsZW1lbnQuaW5uZXJXaWR0aDtcbiAgfVxuXG5cbiAgdXBsb2FkRmlsZShldmVudDogYW55KSB7XG4gICAgdGhpcy5lbWl0RmlsZS5lbWl0KGV2ZW50LnRhcmdldC5maWxlc1swXSk7XG4gIH1cblxuICBvbkRvd25sb2FkKCkge1xuICAgIHRoaXMuZW1pdERvY1VybC5lbWl0KHRoaXMuZmlsZURvd25sb2FkU3RyaW5nKTtcbiAgfVxuXG59XG4iLCI8ZGl2IGNsYXNzPVwidXBsb2FkLWJ0bi13cmFwcGVyXCI+XG4gICAgPGRpdiBjbGFzcz1cInVwbG9hZC1idG5cIiAjZG9jd3JhcHBlcj5cbiAgICAgICAgPGRpdiBjbGFzcz1cImZpbGUtaWNvblwiIChjbGljayk9J3RoaXMuZmlsZS5jbGljaygpJz5cbiAgICAgICAgICAgIDwhLS1UT0RPOiByZW5kZXIgaWNvbiBiYXNlZCBvbiB1cGxvYWRlZCBmaWxlIGV4dGVuc2lvbi0tPlxuICAgICAgICAgICAgPHNwYW4+XG4gICAgICAgICAgICAgICAgPGkgY2xhc3M9XCJtYXRlcmlhbC1pY29ucy1vdXRsaW5lZFwiIHN0eWxlPVwiY29sb3I6IHJnYmEoMCwgMCwgMCwgMC41NCk7XCI+XG4gICAgICAgICAgICAgICAgICAgIGluc2VydF9waG90b1xuICAgICAgICAgICAgICAgIDwvaT4gXG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzPVwiZmlsZS1uYW1lXCIgKGNsaWNrKT0ndGhpcy5maWxlLmNsaWNrKCknPlxuICAgICAgICAgICAge3tmaWxlTmFtZX19XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8ZGl2IGNsYXNzPVwiYnRuXCIgW25nQ2xhc3NdPVwiZmlsZVVwbG9hZFN0YXR1c1wiPlxuICAgICAgICAgICAgPHNwYW4gKm5nSWY9XCJpc1VwbG9hZE5lZWRlZFwiIChjbGljayk9J3RoaXMuZmlsZS5jbGljaygpJz5cbiAgICAgICAgICAgICAgICA8aSBjbGFzcz1cIm1hdGVyaWFsLWljb25zLW91dGxpbmVkXCIgc3R5bGU9XCJjb2xvcjogIzIxOTZGMztcIj5jbG91ZF91cGxvYWQ8L2k+XG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICA8c3BhbiAqbmdJZj1cImZpbGVEb3dubG9hZFN0cmluZ1wiPlxuICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnMtb3V0bGluZWRcIiBzdHlsZT1cImNvbG9yOiAjMjE5NkYzO1wiPmNsb3VkX2Rvd25sb2FkPC9pPlxuICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgPHNwYW4gKm5nSWY9XCJmaWxlVXBsb2FkU3RhdHVzID09PSAndXBsb2FkaW5nJ1wiPlxuICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnNcIiBzdHlsZT1cImNvbG9yOiAjRTY1MTAwO1wiPmNhbmNlbDwvaT5cbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDxzcGFuICpuZ0lmPVwiZmlsZURvd25sb2FkU3RyaW5nICYmIGlzRGVsZXRlQWxsb3dlZFwiPlxuICAgICAgICAgICAgICAgIDxpIGNsYXNzPVwibWF0ZXJpYWwtaWNvbnNcIiBzdHlsZT1cImNvbG9yOiAjRTY1MTAwO1wiPmRlbGV0ZTwvaT5cbiAgICAgICAgICAgIDwvc3Bhbj5cbiAgICAgICAgICAgIDxzcGFuICpuZ0lmPVwiZmlsZURvd25sb2FkU3RyaW5nICYmIGlzRG9jVmVyaWZpZWRcIj5cbiAgICAgICAgICAgICAgICA8aSBjbGFzcz1cIm1hdGVyaWFsLWljb25zXCIgc3R5bGU9XCJjb2xvcjogIzI1QkQxNztcIj5jaGVja19jaXJjbGU8L2k+XG4gICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICA8aW5wdXQgdHlwZT1cImZpbGVcIiBjbGFzcz1cImlucHV0XCIgIG5hbWU9XCJmaWVsZE5hbWVcIiBbZGlzYWJsZWRdPVwiIWlzVXBsb2FkTmVlZGVkXCIgW2FjY2VwdF09XCJhY2NlcHRcIiAoY2hhbmdlKT1cInVwbG9hZEZpbGUoJGV2ZW50KVwiXG4gICAgICAgICAgICAjZmlsZSAvPlxuICAgIDwvZGl2PlxuICAgIDxkaXYgY2xhc3M9XCJwcm9ncmVzc1wiICpuZ0lmPVwiZmlsZVVwbG9hZFN0YXR1cyA9PT0gJ3VwbG9hZGluZydcIj5cbiAgICAgICAgPGRpdiBjbGFzcz1cImluZGV0ZXJtaW5hdGVcIj48L2Rpdj5cbiAgICA8L2Rpdj5cbjwvZGl2PiJdfQ==