import { NgModule } from '@angular/core';
import { DocUploadLibComponent } from './doc-upload-lib.component';
import { CommonModule } from '@angular/common';
import * as i0 from "@angular/core";
var DocUploadLibModule = /** @class */ (function () {
    function DocUploadLibModule() {
    }
    DocUploadLibModule.ɵmod = i0.ɵɵdefineNgModule({ type: DocUploadLibModule });
    DocUploadLibModule.ɵinj = i0.ɵɵdefineInjector({ factory: function DocUploadLibModule_Factory(t) { return new (t || DocUploadLibModule)(); }, imports: [[
                CommonModule
            ]] });
    return DocUploadLibModule;
}());
export { DocUploadLibModule };
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && i0.ɵɵsetNgModuleScope(DocUploadLibModule, { declarations: [DocUploadLibComponent], imports: [CommonModule], exports: [DocUploadLibComponent] }); })();
/*@__PURE__*/ (function () { i0.ɵsetClassMetadata(DocUploadLibModule, [{
        type: NgModule,
        args: [{
                declarations: [DocUploadLibComponent],
                imports: [
                    CommonModule
                ],
                exports: [DocUploadLibComponent]
            }]
    }], null, null); })();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG9jLXVwbG9hZC1saWIubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZG9jLXVwbG9hZC1saWIvIiwic291cmNlcyI6WyJsaWIvZG9jLXVwbG9hZC1saWIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDbkUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDOztBQUkvQztJQUFBO0tBT21DOzBEQUF0QixrQkFBa0I7dUhBQWxCLGtCQUFrQixrQkFMcEI7Z0JBQ1AsWUFBWTthQUNiOzZCQVZIO0NBYW1DLEFBUG5DLElBT21DO1NBQXRCLGtCQUFrQjt3RkFBbEIsa0JBQWtCLG1CQU5kLHFCQUFxQixhQUVsQyxZQUFZLGFBRUoscUJBQXFCO2tEQUVwQixrQkFBa0I7Y0FQOUIsUUFBUTtlQUFDO2dCQUNSLFlBQVksRUFBRSxDQUFDLHFCQUFxQixDQUFDO2dCQUNyQyxPQUFPLEVBQUU7b0JBQ1AsWUFBWTtpQkFDYjtnQkFDRCxPQUFPLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQzthQUNqQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBEb2NVcGxvYWRMaWJDb21wb25lbnQgfSBmcm9tICcuL2RvYy11cGxvYWQtbGliLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuXG5cblxuQE5nTW9kdWxlKHtcbiAgZGVjbGFyYXRpb25zOiBbRG9jVXBsb2FkTGliQ29tcG9uZW50XSxcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZVxuICBdLFxuICBleHBvcnRzOiBbRG9jVXBsb2FkTGliQ29tcG9uZW50XVxufSlcbmV4cG9ydCBjbGFzcyBEb2NVcGxvYWRMaWJNb2R1bGUgeyB9XG4iXX0=