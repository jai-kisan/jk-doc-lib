import * as i0 from "@angular/core";
import * as i1 from "./doc-upload-lib.component";
import * as i2 from "@angular/common";
export declare class DocUploadLibModule {
    static ɵmod: i0.ɵɵNgModuleDefWithMeta<DocUploadLibModule, [typeof i1.DocUploadLibComponent], [typeof i2.CommonModule], [typeof i1.DocUploadLibComponent]>;
    static ɵinj: i0.ɵɵInjectorDef<DocUploadLibModule>;
}
