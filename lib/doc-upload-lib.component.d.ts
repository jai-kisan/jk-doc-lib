import { OnInit, ElementRef, EventEmitter } from '@angular/core';
import * as i0 from "@angular/core";
export declare class DocUploadLibComponent implements OnInit {
    inputWidth: number;
    elementProgress: ElementRef;
    docWrapper: ElementRef;
    isUploadNeeded: boolean;
    isDeleteAllowed: boolean;
    isDocVerified: boolean;
    fileName: string;
    fileUploadStatus: string;
    fileDownloadString: string;
    accept: string;
    emitFile: EventEmitter<File>;
    emitDocUrl: EventEmitter<string>;
    constructor();
    ngOnInit(): void;
    ngAfterViewInit(): void;
    uploadFile(event: any): void;
    onDownload(): void;
    static ɵfac: i0.ɵɵFactoryDef<DocUploadLibComponent, never>;
    static ɵcmp: i0.ɵɵComponentDefWithMeta<DocUploadLibComponent, "doc-doc-upload-lib", never, { "isUploadNeeded": "isUploadNeeded"; "isDeleteAllowed": "isDeleteAllowed"; "isDocVerified": "isDocVerified"; "fileName": "fileName"; "fileUploadStatus": "fileUploadStatus"; "fileDownloadString": "fileDownloadString"; "accept": "accept"; }, { "emitFile": "emitFile"; "emitDocUrl": "emitDocUrl"; }, never, never>;
}
