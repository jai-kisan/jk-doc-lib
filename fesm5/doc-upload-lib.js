import { ɵɵdefineInjectable, ɵsetClassMetadata, Injectable, ɵɵgetCurrentView, ɵɵelementStart, ɵɵlistener, ɵɵrestoreView, ɵɵnextContext, ɵɵreference, ɵɵtext, ɵɵelementEnd, ɵɵelement, EventEmitter, ɵɵdefineComponent, ɵɵviewQuery, ɵɵqueryRefresh, ɵɵloadQuery, ɵɵtemplate, ɵɵadvance, ɵɵtextInterpolate1, ɵɵproperty, Component, ViewChild, Input, Output, ɵɵdefineNgModule, ɵɵdefineInjector, ɵɵsetNgModuleScope, NgModule } from '@angular/core';
import { NgClass, NgIf, CommonModule } from '@angular/common';

var DocUploadLibService = /** @class */ (function () {
    function DocUploadLibService() {
    }
    DocUploadLibService.ɵfac = function DocUploadLibService_Factory(t) { return new (t || DocUploadLibService)(); };
    DocUploadLibService.ɵprov = ɵɵdefineInjectable({ token: DocUploadLibService, factory: DocUploadLibService.ɵfac, providedIn: 'root' });
    return DocUploadLibService;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(DocUploadLibService, [{
        type: Injectable,
        args: [{
                providedIn: 'root'
            }]
    }], function () { return []; }, null); })();

var _c0 = ["progressbar"];
var _c1 = ["docwrapper"];
function DocUploadLibComponent_span_10_Template(rf, ctx) { if (rf & 1) {
    var _r9 = ɵɵgetCurrentView();
    ɵɵelementStart(0, "span", 12);
    ɵɵlistener("click", function DocUploadLibComponent_span_10_Template_span_click_0_listener() { ɵɵrestoreView(_r9); ɵɵnextContext(); var _r6 = ɵɵreference(16); return _r6.click(); });
    ɵɵelementStart(1, "i", 13);
    ɵɵtext(2, "cloud_upload");
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
function DocUploadLibComponent_span_11_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span");
    ɵɵelementStart(1, "i", 13);
    ɵɵtext(2, "cloud_download");
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
function DocUploadLibComponent_span_12_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span");
    ɵɵelementStart(1, "i", 14);
    ɵɵtext(2, "cancel");
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
function DocUploadLibComponent_span_13_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span");
    ɵɵelementStart(1, "i", 14);
    ɵɵtext(2, "delete");
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
function DocUploadLibComponent_span_14_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "span");
    ɵɵelementStart(1, "i", 15);
    ɵɵtext(2, "check_circle");
    ɵɵelementEnd();
    ɵɵelementEnd();
} }
function DocUploadLibComponent_div_17_Template(rf, ctx) { if (rf & 1) {
    ɵɵelementStart(0, "div", 16);
    ɵɵelement(1, "div", 17);
    ɵɵelementEnd();
} }
var DocUploadLibComponent = /** @class */ (function () {
    function DocUploadLibComponent() {
        this.isUploadNeeded = true;
        this.isDeleteAllowed = true;
        this.isDocVerified = false;
        this.emitFile = new EventEmitter();
        this.emitDocUrl = new EventEmitter();
    }
    DocUploadLibComponent.prototype.ngOnInit = function () {
    };
    DocUploadLibComponent.prototype.ngAfterViewInit = function () {
        this.inputWidth = this.docWrapper.nativeElement.innerWidth;
    };
    DocUploadLibComponent.prototype.uploadFile = function (event) {
        this.emitFile.emit(event.target.files[0]);
    };
    DocUploadLibComponent.prototype.onDownload = function () {
        this.emitDocUrl.emit(this.fileDownloadString);
    };
    DocUploadLibComponent.ɵfac = function DocUploadLibComponent_Factory(t) { return new (t || DocUploadLibComponent)(); };
    DocUploadLibComponent.ɵcmp = ɵɵdefineComponent({ type: DocUploadLibComponent, selectors: [["doc-doc-upload-lib"]], viewQuery: function DocUploadLibComponent_Query(rf, ctx) { if (rf & 1) {
            ɵɵviewQuery(_c0, true);
            ɵɵviewQuery(_c1, true);
        } if (rf & 2) {
            var _t;
            ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.elementProgress = _t.first);
            ɵɵqueryRefresh(_t = ɵɵloadQuery()) && (ctx.docWrapper = _t.first);
        } }, inputs: { isUploadNeeded: "isUploadNeeded", isDeleteAllowed: "isDeleteAllowed", isDocVerified: "isDocVerified", fileName: "fileName", fileUploadStatus: "fileUploadStatus", fileDownloadString: "fileDownloadString", accept: "accept" }, outputs: { emitFile: "emitFile", emitDocUrl: "emitDocUrl" }, decls: 18, vars: 10, consts: [[1, "upload-btn-wrapper"], [1, "upload-btn"], ["docwrapper", ""], [1, "file-icon", 3, "click"], [1, "material-icons-outlined", 2, "color", "rgba(0, 0, 0, 0.54)"], [1, "file-name", 3, "click"], [1, "btn", 3, "ngClass"], [3, "click", 4, "ngIf"], [4, "ngIf"], ["type", "file", "name", "fieldName", 1, "input", 3, "disabled", "accept", "change"], ["file", ""], ["class", "progress", 4, "ngIf"], [3, "click"], [1, "material-icons-outlined", 2, "color", "#2196F3"], [1, "material-icons", 2, "color", "#E65100"], [1, "material-icons", 2, "color", "#25BD17"], [1, "progress"], [1, "indeterminate"]], template: function DocUploadLibComponent_Template(rf, ctx) { if (rf & 1) {
            var _r10 = ɵɵgetCurrentView();
            ɵɵelementStart(0, "div", 0);
            ɵɵelementStart(1, "div", 1, 2);
            ɵɵelementStart(3, "div", 3);
            ɵɵlistener("click", function DocUploadLibComponent_Template_div_click_3_listener() { ɵɵrestoreView(_r10); var _r6 = ɵɵreference(16); return _r6.click(); });
            ɵɵelementStart(4, "span");
            ɵɵelementStart(5, "i", 4);
            ɵɵtext(6, " insert_photo ");
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵelementStart(7, "div", 5);
            ɵɵlistener("click", function DocUploadLibComponent_Template_div_click_7_listener() { ɵɵrestoreView(_r10); var _r6 = ɵɵreference(16); return _r6.click(); });
            ɵɵtext(8);
            ɵɵelementEnd();
            ɵɵelementStart(9, "div", 6);
            ɵɵtemplate(10, DocUploadLibComponent_span_10_Template, 3, 0, "span", 7);
            ɵɵtemplate(11, DocUploadLibComponent_span_11_Template, 3, 0, "span", 8);
            ɵɵtemplate(12, DocUploadLibComponent_span_12_Template, 3, 0, "span", 8);
            ɵɵtemplate(13, DocUploadLibComponent_span_13_Template, 3, 0, "span", 8);
            ɵɵtemplate(14, DocUploadLibComponent_span_14_Template, 3, 0, "span", 8);
            ɵɵelementEnd();
            ɵɵelementStart(15, "input", 9, 10);
            ɵɵlistener("change", function DocUploadLibComponent_Template_input_change_15_listener($event) { return ctx.uploadFile($event); });
            ɵɵelementEnd();
            ɵɵelementEnd();
            ɵɵtemplate(17, DocUploadLibComponent_div_17_Template, 2, 0, "div", 11);
            ɵɵelementEnd();
        } if (rf & 2) {
            ɵɵadvance(8);
            ɵɵtextInterpolate1(" ", ctx.fileName, " ");
            ɵɵadvance(1);
            ɵɵproperty("ngClass", ctx.fileUploadStatus);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.isUploadNeeded);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.fileDownloadString);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.fileUploadStatus === "uploading");
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.fileDownloadString && ctx.isDeleteAllowed);
            ɵɵadvance(1);
            ɵɵproperty("ngIf", ctx.fileDownloadString && ctx.isDocVerified);
            ɵɵadvance(1);
            ɵɵproperty("disabled", !ctx.isUploadNeeded)("accept", ctx.accept);
            ɵɵadvance(2);
            ɵɵproperty("ngIf", ctx.fileUploadStatus === "uploading");
        } }, directives: [NgClass, NgIf], styles: [".upload-btn-wrapper[_ngcontent-%COMP%]{\n      position: relative;\n      display: inline-block;\n    }\n    .upload-btn[_ngcontent-%COMP%] {\n      position: relative;\n      overflow: hidden;\n      display: inline-flex;\n      flex-direction: row;\n      margin: 5px;\n      cursor: pointer;\n      background: rgba(0, 0, 0, 0.04);\n      border-radius: 4px;\n      height: 40px;\n      padding: 7px;\n      box-sizing: border-box;\n      background: #E3F2FD;\n    }\n    .file-icon[_ngcontent-%COMP%] {\n      margin-right: 5px;\n      align-self: center;\n      z-index: 1;\n    }\n    .file-icon[_ngcontent-%COMP%]   i[_ngcontent-%COMP%] {\n      font-size: 26px;\n      height: 26px;\n      width: 26px;\n    }\n    .file-name[_ngcontent-%COMP%] {\n      margin-right: 5px;\n      text-overflow: ellipsis;\n      align-self: center;\n      flex: 1;\n      box-sizing: border-box;\n      padding-bottom: 4px;\n      font-size: 12px;\n      line-height: 20px;\n      letter-spacing: 0.25px;\n      color: #828282;\n      mix-blend-mode: normal;\n      z-index: 1;\n    }\n  .upload-btn[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%] {\n    align-self: center;\n      margin-left: 5px;\n      color: #2196f3;\n      z-index: 1;\n  }\n  .upload-btn[_ngcontent-%COMP%]   .btn[_ngcontent-%COMP%]   span[_ngcontent-%COMP%]{\n    margin: 5px;\n    z-index: 1;\n  }\n  \n  .upload-anim[_ngcontent-%COMP%]{\n    height: 2px;\n    position: relative;\n    left: 0px;\n    top: -8px;\n    border-radius: 4px;\n    box-sizing: border-box;\n    height: 2px;\n    background-image: linear-gradient(\n      90deg,\n      rgba(227, 242, 253, 1) 0%,\n      rgba(100, 181, 246, 1) 45%,\n      rgba(100, 181, 246, 1) 45%\n    );\n    animation: move 2s linear infinite;\n  }\n  @keyframes move {\n    0% {\n      background-position: 0 0;\n    }\n    100% {\n      background-position: 250px 250px;\n    }\n  }\n  .input[_ngcontent-%COMP%] {\n      font-size: 100px;\n      position: absolute;\n      left: 0;\n      top: 0;\n      opacity: 0;\n  }\n  .progress[_ngcontent-%COMP%] {\n    position: relative;\n    top: -7px;\n    height: 2px;\n    display: block;\n    width: calc( 100% - 10px );\n    border-radius: 2px;\n    box-sizing: border-box;\n    background-clip: padding-box;\n    overflow: hidden;\n    margin-left: 5px; \n  }\n  .progress[_ngcontent-%COMP%]   .indeterminate[_ngcontent-%COMP%] {\n      background-color: #2196F3; \n  }\n  .progress[_ngcontent-%COMP%]   .indeterminate[_ngcontent-%COMP%]:before {\n    content: '';\n    position: absolute;\n    background-color: inherit;\n    top: 0;\n    left: 0;\n    bottom: 0;\n    will-change: left, right;\n    -webkit-animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite;\n            animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite; }\n.progress[_ngcontent-%COMP%]   .indeterminate[_ngcontent-%COMP%]:after {\n    content: '';\n    position: absolute;\n    background-color: inherit;\n    top: 0;\n    left: 0;\n    bottom: 0;\n    will-change: left, right;\n    -webkit-animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n            animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n    -webkit-animation-delay: 1.15s;\n            animation-delay: 1.15s; }\n            @-webkit-keyframes indeterminate {\n              0% {\n                left: -35%;\n                right: 100%; }\n              60% {\n                left: 100%;\n                right: -90%; }\n              100% {\n                left: 100%;\n                right: -90%; } }\n            @keyframes indeterminate {\n              0% {\n                left: -35%;\n                right: 100%; }\n              60% {\n                left: 100%;\n                right: -90%; }\n              100% {\n                left: 100%;\n                right: -90%; } }\n            @-webkit-keyframes indeterminate-short {\n              0% {\n                left: -200%;\n                right: 100%; }\n              60% {\n                left: 107%;\n                right: -8%; }\n              100% {\n                left: 107%;\n                right: -8%; } }\n            @keyframes indeterminate-short {\n              0% {\n                left: -200%;\n                right: 100%; }\n              60% {\n                left: 107%;\n                right: -8%; }\n              100% {\n                left: 107%;\n                right: -8%; } }"] });
    return DocUploadLibComponent;
}());
/*@__PURE__*/ (function () { ɵsetClassMetadata(DocUploadLibComponent, [{
        type: Component,
        args: [{
                selector: 'doc-doc-upload-lib',
                templateUrl: './doc-upload-lib.component.html',
                styles: [
                    ".upload-btn-wrapper{\n      position: relative;\n      display: inline-block;\n    }\n    .upload-btn {\n      position: relative;\n      overflow: hidden;\n      display: inline-flex;\n      flex-direction: row;\n      margin: 5px;\n      cursor: pointer;\n      background: rgba(0, 0, 0, 0.04);\n      border-radius: 4px;\n      height: 40px;\n      padding: 7px;\n      box-sizing: border-box;\n      background: #E3F2FD;\n    }\n    .file-icon {\n      margin-right: 5px;\n      align-self: center;\n      z-index: 1;\n    }\n    .file-icon i {\n      font-size: 26px;\n      height: 26px;\n      width: 26px;\n    }\n    .file-name {\n      margin-right: 5px;\n      text-overflow: ellipsis;\n      align-self: center;\n      flex: 1;\n      box-sizing: border-box;\n      padding-bottom: 4px;\n      font-size: 12px;\n      line-height: 20px;\n      letter-spacing: 0.25px;\n      color: #828282;\n      mix-blend-mode: normal;\n      z-index: 1;\n    }\n  .upload-btn .btn {\n    align-self: center;\n      margin-left: 5px;\n      color: #2196f3;\n      z-index: 1;\n  }\n  .upload-btn .btn span{\n    margin: 5px;\n    z-index: 1;\n  }\n  \n  .upload-anim{\n    height: 2px;\n    position: relative;\n    left: 0px;\n    top: -8px;\n    border-radius: 4px;\n    box-sizing: border-box;\n    height: 2px;\n    background-image: linear-gradient(\n      90deg,\n      rgba(227, 242, 253, 1) 0%,\n      rgba(100, 181, 246, 1) 45%,\n      rgba(100, 181, 246, 1) 45%\n    );\n    animation: move 2s linear infinite;\n  }\n  @keyframes move {\n    0% {\n      background-position: 0 0;\n    }\n    100% {\n      background-position: 250px 250px;\n    }\n  }\n  .input {\n      font-size: 100px;\n      position: absolute;\n      left: 0;\n      top: 0;\n      opacity: 0;\n  }\n  .progress {\n    position: relative;\n    top: -7px;\n    height: 2px;\n    display: block;\n    width: calc( 100% - 10px );\n    border-radius: 2px;\n    box-sizing: border-box;\n    background-clip: padding-box;\n    overflow: hidden;\n    margin-left: 5px; \n  }\n  .progress .indeterminate {\n      background-color: #2196F3; \n  }\n  .progress .indeterminate:before {\n    content: '';\n    position: absolute;\n    background-color: inherit;\n    top: 0;\n    left: 0;\n    bottom: 0;\n    will-change: left, right;\n    -webkit-animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite;\n            animation: indeterminate 2.1s cubic-bezier(0.65, 0.815, 0.735, 0.395) infinite; }\n.progress .indeterminate:after {\n    content: '';\n    position: absolute;\n    background-color: inherit;\n    top: 0;\n    left: 0;\n    bottom: 0;\n    will-change: left, right;\n    -webkit-animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n            animation: indeterminate-short 2.1s cubic-bezier(0.165, 0.84, 0.44, 1) infinite;\n    -webkit-animation-delay: 1.15s;\n            animation-delay: 1.15s; }\n            @-webkit-keyframes indeterminate {\n              0% {\n                left: -35%;\n                right: 100%; }\n              60% {\n                left: 100%;\n                right: -90%; }\n              100% {\n                left: 100%;\n                right: -90%; } }\n            @keyframes indeterminate {\n              0% {\n                left: -35%;\n                right: 100%; }\n              60% {\n                left: 100%;\n                right: -90%; }\n              100% {\n                left: 100%;\n                right: -90%; } }\n            @-webkit-keyframes indeterminate-short {\n              0% {\n                left: -200%;\n                right: 100%; }\n              60% {\n                left: 107%;\n                right: -8%; }\n              100% {\n                left: 107%;\n                right: -8%; } }\n            @keyframes indeterminate-short {\n              0% {\n                left: -200%;\n                right: 100%; }\n              60% {\n                left: 107%;\n                right: -8%; }\n              100% {\n                left: 107%;\n                right: -8%; } }\n  "
                ]
            }]
    }], function () { return []; }, { elementProgress: [{
            type: ViewChild,
            args: ['progressbar', { static: false }]
        }], docWrapper: [{
            type: ViewChild,
            args: ['docwrapper', { static: false }]
        }], isUploadNeeded: [{
            type: Input
        }], isDeleteAllowed: [{
            type: Input
        }], isDocVerified: [{
            type: Input
        }], fileName: [{
            type: Input
        }], fileUploadStatus: [{
            type: Input
        }], fileDownloadString: [{
            type: Input
        }], accept: [{
            type: Input
        }], emitFile: [{
            type: Output
        }], emitDocUrl: [{
            type: Output
        }] }); })();

var DocUploadLibModule = /** @class */ (function () {
    function DocUploadLibModule() {
    }
    DocUploadLibModule.ɵmod = ɵɵdefineNgModule({ type: DocUploadLibModule });
    DocUploadLibModule.ɵinj = ɵɵdefineInjector({ factory: function DocUploadLibModule_Factory(t) { return new (t || DocUploadLibModule)(); }, imports: [[
                CommonModule
            ]] });
    return DocUploadLibModule;
}());
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && ɵɵsetNgModuleScope(DocUploadLibModule, { declarations: [DocUploadLibComponent], imports: [CommonModule], exports: [DocUploadLibComponent] }); })();
/*@__PURE__*/ (function () { ɵsetClassMetadata(DocUploadLibModule, [{
        type: NgModule,
        args: [{
                declarations: [DocUploadLibComponent],
                imports: [
                    CommonModule
                ],
                exports: [DocUploadLibComponent]
            }]
    }], null, null); })();

/*
 * Public API Surface of doc-upload-lib
 */

/**
 * Generated bundle index. Do not edit.
 */

export { DocUploadLibComponent, DocUploadLibModule, DocUploadLibService };
//# sourceMappingURL=doc-upload-lib.js.map
